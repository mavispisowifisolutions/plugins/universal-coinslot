"use strict"
/*
* Plugin Name: Universal Coin Acceptor
* Author: Reigel Gallarde
* version: 1.4.5
* Description: A script to accept coins using Universal Coin Acceptor.
*/

// data init
var defaultCoins = [{
  id: 1,
  pulses: 1,
  credits: 1,
  time: 300,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  id: 2,
  pulses: 5,
  credits: 5,
  time: 1800,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  id: 3,
  pulses: 10,
  credits: 10,
  time: 3600,
  createdAt: new Date(),
  updatedAt: new Date()
}];
var cointable = {};
var macs = [];
var macs_queue = [];
var macs_turn = {};
var active_mac = null;
var added_time = 0;
var added_credits = 0;
var insert_coin_distance; 
var pulse_pin;
var penalty_time1;
var penalty_time2;
var timer; 
var coindrop;
var credits = 0;
var relay_pin;
var relay_delay;
var relay_state;
var smart;
var pulse_distribution;
var coin_bounce_rate = 500;

var coinwire;
var solenoid;

var dropTT;
var addTimeTT;

get_option('insert-coin-distance').then(d => { insert_coin_distance = parseInt(d); return d; })
get_option('insert-coin-wait-time').then(d => { timer = parseInt(d); return d; })
get_option('insert-coin-pin1').then(d => { 
  pulse_pin = d; 
  if (pulse_pin) {
    coinwire = new Gpio(pulse_pin, 'in', 'falling');
  }
  return d;
})

get_option('insert-coin-penalty-time1').then(d => { penalty_time1 = parseInt(d); return d; })
get_option('insert-coin-penalty-time2').then(d => { penalty_time2 = parseInt(d); return d; })
get_option('insert-coin-relay-pin').then(d => { 
  relay_pin = d;
  if (relay_pin) {
    solenoid = new Gpio(relay_pin, 'out');
    get_option('insert-coin-relay-state').then(relay_state => {
      solenoid.setActiveLow(relay_state==0);
      solenoid.writeSync(0);
      return relay_state; 
    })
  }
  return d;
})
get_option('insert-coin-relay-delay').then(d => { relay_delay = d; return d; })
get_option('insert-coin-smart').then(d => { smart = d == 'yes'; return d; })
get_option('insert-coin-pulse-distribution').then(d => { 
  pulse_distribution = d;

  var msg = {
    'pulse_distribution': pulse_distribution,
  };
  pisowifi.events.emit('plugin_options', msg);
  if (pulse_distribution == 'accumulative') {
    if (!timer) {
      get_option('insert-coin-wait-time').then(d => { 
        timer = parseInt(d); 
        coin_bounce_rate = (timer-2)*1000;
        return d; 
      })
    } else {
      coin_bounce_rate = (timer-2)*1000;
    }
  }
  return d; 
})

var getCredits = function() {
  console.log('get active_mac', active_mac)
  if ( active_mac ) {
    return get_option( 'credits-' + active_mac );
  }
}

var updateCredits = function(mac,credits) {
  if (pulse_distribution !== 'accumulative') {return; }
  if ( mac && !isNaN(credits) ) {
    console.log('set active_mac', mac)
    set_option( 'credits-' + mac, credits ).then(function(c){ 
      console.log('saving credits', credits) 
      return c;
    });
  }
}

var addTime = function( drop ) {
  if ( active_mac && !isNaN(drop) ) {
    credits = 0;
    added_time = 0;
    macs_turn[active_mac].penalty = 0;
    var data = cointable[drop];
    if (!data) {
      var coins = drop.toString().sorter();
      coins.forEach(coin=>{
        data = cointable[coin];
        pisowifi.events.emit('addTime', active_mac, data);
        added_time += data.time;
      })
    } else {
      pisowifi.events.emit('addTime', active_mac, data);
      added_time += data.time;
    }
    var msg = {
      event: 'insertcoin-timer',
      'added-time': added_time,
    };
    pisowifi.portal.send( active_mac, msg );
  }
}

var coindrop_default = function( ){
  clearTimeout(dropTT);
  clearTimeout(addTimeTT);
  var drop = 0;
  var false_positive = 0;
  var solenoid_cut = false;
  solenoid.writeSync(1);
  var fpTT = setInterval(()=>{
    false_positive += 1;
    if ( false_positive > 2) { 
      clearInterval(fpTT);
    }
  },40);
  coinwire.watch((err, value) => {
    clearTimeout(dropTT);
    clearTimeout(addTimeTT);
    if ( false_positive < 2 ) { console.log('false positive', false_positive);return; }
    if (err) {
      console.log( err );
    } else {
      if (smart && !solenoid_cut) {
        solenoid.writeSync(0);
        solenoid_cut = true;
      }
      if (drop == 0) {
        coindrop = true;
        console.log('coin drop');
      }
      credits++;
      drop++
      console.log(credits);
      dropTT = setTimeout(()=>{
        if (smart && solenoid_cut ) {
          solenoid.writeSync(1);
          solenoid_cut = false;
        }
        pisowifi.events.emit('addSales', active_mac, Number(drop).toFixed(2));
        added_credits = drop;
        drop = 0;
        updateCredits(active_mac,credits);
      }, 500 );
      addTimeTT = setTimeout(()=>{
        addTime( credits );
        updateCredits(active_mac,credits);
      },coin_bounce_rate)
    }
  });
}

var coindrop_heartbeat = function() {
  // coin drop heartbeat
  var running = false;
  
  this.start = function(){
    if ( !running  ) {
      running = true;
      console.log('waiting for coins...');
      getCredits().then(function(c){
        console.log('save credits', c)
        credits = parseInt(c) || 0;
        added_credits = credits;
        coindrop_default();
      })
    }
  }
  this.stop = function(callback){
    if (credits) {
      addTime( credits );
      updateCredits(active_mac, credits);
    }
    solenoid.writeSync(0);
    coinwire.unwatch();
    console.log('done waiting for coins...')
    added_time = 0;
    added_credits = 0;
    active_mac = null;
    running = false;
    if (typeof callback == 'function') {
      callback();
    }
  }
  this.running = function(){
    return running;
  }
}
/*mac_lookup = () => {
  return {signal: 20}
}*/
var is_device_near = function ( mac ) {
  var lookup = mac_lookup( mac );
  return ( insert_coin_distance == 0 ) || ( insert_coin_distance > lookup.signal ) 
} 

var fn_heartbeat = function(){
  var timer_id;
  var running = false;
  
  this.start = function(){
    if ( !running  ) {
      running = true;
      timer_id = setInterval(function(){
        
        if ( macs.length ) {
          macs.forEach(function( mac ){
            var lookup = mac_lookup( mac );
            macs_turn[mac] = macs_turn[mac] || {};
            if (macs_turn[mac].penalty === undefined) {
              macs_turn[mac].penalty = 0;
            }
            if ( macs_turn[mac].penalty > 0 ) {
              macs_turn[mac].penalty--;
            }
            var msg = {
              event: 'heartbeat',
              distance: {
                max: insert_coin_distance, 
                current: lookup.signal,
              },
              myturn: macs_turn[mac].penalty || !active_mac || !coinwire.iswatched(),
            };
            pisowifi.portal.send( mac, msg );  
          })      
        }
      },1000)
    }
  }
  this.stop = function(){
    clearInterval(timer_id);
    running = false;
  }
  this.running = function(){
    return running;
  }
}

var heartbeat = new fn_heartbeat();

db.cointable.findAll({raw:true})
  .then(result=>{
    
    if (result && result.length ) {
      return result;
    } else {
      var queryInterface = db.sequelize.getQueryInterface();
      return queryInterface.bulkInsert('cointables', defaultCoins, {})
      .then(()=> db.cointable.findAll({raw:true}) );
    }
  })
  .then(result => {
    if (result) {
      result.forEach(function(data){
        cointable[data.pulses] = data;
      });

      var msg = {
        'cointable': cointable,
      };
      pisowifi.events.emit('plugin_options', msg);
    }

  });

String.prototype.sorter = function( ){
  var bill = parseInt(this,10);
  var sorted = [];
  var coins = Object.keys(cointable)
  var index = coins.length;
  
  while ( (index != -1) && (bill > 0.99) ) {
    if (bill >= coins[index]) {
      bill -= coins[index]
      sorted.push(coins[index])
    } else {
      index -= 1;
    }
  }
  return sorted;
}

var tt;
var coindropd = new coindrop_heartbeat();
pisowifi.events.on('insertcoin', ( data, target ) => {
  var max = timer*1;
  var counter = timer*1;
  var total_counter = 0;
  macs_turn[data.mac] = macs_turn[data.mac] || {}
  if ( ( data.request === true ) && !coinwire.iswatched() && !active_mac && !macs_turn[data.mac].penalty) {
    clearInterval(tt);
    active_mac = data.mac;
    coindropd.start();
    tt = setInterval( function(){
      if (coindrop) {counter = timer*1;}
      var msg = {
        event: 'insertcoin-timer',
        max: max,
        time: counter--,
        'added-credits': added_credits,
        coindrop: coindrop
      };
      total_counter++;
      pisowifi.portal.send( data.mac, msg );
      if (coindrop) {coindrop = false;}
      if ((counter == 0) || !active_mac ) {
        clearInterval(tt);
        coindropd.stop();
      }
      if ( (counter == 0 ) && (total_counter + counter == max ) ) {
        macs_turn[data.mac].penalty += penalty_time2;
      }
      added_credits = 0;
      added_time = 0;
    }, 1000)
  } else {
    macs_turn[data.mac].penalty += penalty_time1;
    clearInterval(tt);
    coindropd.stop();
  }
})


pisowifi.events.on('socket-add', ( data, target ) => {
  if ( macs.indexOf(data.mac) == -1) {
    macs.push(data.mac)
    heartbeat.start();
  }
  //console.log('socket-add', target, data)
})
pisowifi.events.on('socket-remove', ( data, target ) => {
  if ( macs.indexOf(data.mac) != -1) {
    macs.splice(macs.indexOf(data.mac),1)
  }
  if (( data.mac == active_mac) && data.empty ) {
    active_mac = null;
  }
  if ( !macs.length && !active_mac) {
    heartbeat.stop();
  }
  //console.log('socket-remove', target, data)
})